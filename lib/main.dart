import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:testing_project/features/bonus/bonus_routes.dart';
import 'package:testing_project/features/currency/currency_routes.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:testing_project/features/home/home_routes.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  await dotenv.load(fileName: ".env");
  await initializeDateFormatting('th', null);
  runApp(const AppBloc());
}

/// Setting BLOC here :>
class AppBloc extends StatelessWidget {
  const AppBloc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return MultiBlocProvider(
    //   providers: [
    //     /// Add bloc provider here
    //     BlocProvider<ExampleBloc>(
    //       create: (context) => ExampleBloc(),
    //     ),
    //   ],
    //   child: const AppRouter(),
    // );

    return const AppRouter();
  }
}

class AppRouter extends StatelessWidget {
  const AppRouter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,

      /// Example Navigator.pushNamed( context, '/' );
      routes: {
        ...homeRoutes,
        ...currencyRoutes,
        ...bonusRoutes,
      },
    );
  }
}
