import 'package:flutter/material.dart';
import 'package:testing_project/main.dart';

class AppScaffold extends StatelessWidget {
  const AppScaffold({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF252429),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            if (navigatorKey.currentState!.canPop())
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () => navigatorKey.currentState?.pop(),
                    child: const Icon(
                      Icons.arrow_back_rounded,
                      size: 30.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            Expanded(
              child: child,
            ),
          ],
        ),
      ),
    );
  }
}
