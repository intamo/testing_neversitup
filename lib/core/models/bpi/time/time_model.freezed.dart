// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'time_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TimeModel _$TimeModelFromJson(Map<String, dynamic> json) {
  return _TimeModel.fromJson(json);
}

/// @nodoc
mixin _$TimeModel {
  CurrencyType get code => throw _privateConstructorUsedError;
  String get updated => throw _privateConstructorUsedError;
  String get updatedISO => throw _privateConstructorUsedError;
  @JsonKey(name: 'updateduk')
  String get updateDuk => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TimeModelCopyWith<TimeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TimeModelCopyWith<$Res> {
  factory $TimeModelCopyWith(TimeModel value, $Res Function(TimeModel) then) =
      _$TimeModelCopyWithImpl<$Res, TimeModel>;
  @useResult
  $Res call(
      {CurrencyType code,
      String updated,
      String updatedISO,
      @JsonKey(name: 'updateduk') String updateDuk});
}

/// @nodoc
class _$TimeModelCopyWithImpl<$Res, $Val extends TimeModel>
    implements $TimeModelCopyWith<$Res> {
  _$TimeModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = null,
    Object? updated = null,
    Object? updatedISO = null,
    Object? updateDuk = null,
  }) {
    return _then(_value.copyWith(
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as CurrencyType,
      updated: null == updated
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
      updatedISO: null == updatedISO
          ? _value.updatedISO
          : updatedISO // ignore: cast_nullable_to_non_nullable
              as String,
      updateDuk: null == updateDuk
          ? _value.updateDuk
          : updateDuk // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_TimeModelCopyWith<$Res> implements $TimeModelCopyWith<$Res> {
  factory _$$_TimeModelCopyWith(
          _$_TimeModel value, $Res Function(_$_TimeModel) then) =
      __$$_TimeModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {CurrencyType code,
      String updated,
      String updatedISO,
      @JsonKey(name: 'updateduk') String updateDuk});
}

/// @nodoc
class __$$_TimeModelCopyWithImpl<$Res>
    extends _$TimeModelCopyWithImpl<$Res, _$_TimeModel>
    implements _$$_TimeModelCopyWith<$Res> {
  __$$_TimeModelCopyWithImpl(
      _$_TimeModel _value, $Res Function(_$_TimeModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? code = null,
    Object? updated = null,
    Object? updatedISO = null,
    Object? updateDuk = null,
  }) {
    return _then(_$_TimeModel(
      code: null == code
          ? _value.code
          : code // ignore: cast_nullable_to_non_nullable
              as CurrencyType,
      updated: null == updated
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
      updatedISO: null == updatedISO
          ? _value.updatedISO
          : updatedISO // ignore: cast_nullable_to_non_nullable
              as String,
      updateDuk: null == updateDuk
          ? _value.updateDuk
          : updateDuk // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TimeModel implements _TimeModel {
  const _$_TimeModel(
      {this.code = CurrencyType.USD,
      this.updated = '',
      this.updatedISO = '',
      @JsonKey(name: 'updateduk') this.updateDuk = ''});

  factory _$_TimeModel.fromJson(Map<String, dynamic> json) =>
      _$$_TimeModelFromJson(json);

  @override
  @JsonKey()
  final CurrencyType code;
  @override
  @JsonKey()
  final String updated;
  @override
  @JsonKey()
  final String updatedISO;
  @override
  @JsonKey(name: 'updateduk')
  final String updateDuk;

  @override
  String toString() {
    return 'TimeModel(code: $code, updated: $updated, updatedISO: $updatedISO, updateDuk: $updateDuk)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TimeModel &&
            (identical(other.code, code) || other.code == code) &&
            (identical(other.updated, updated) || other.updated == updated) &&
            (identical(other.updatedISO, updatedISO) ||
                other.updatedISO == updatedISO) &&
            (identical(other.updateDuk, updateDuk) ||
                other.updateDuk == updateDuk));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, code, updated, updatedISO, updateDuk);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_TimeModelCopyWith<_$_TimeModel> get copyWith =>
      __$$_TimeModelCopyWithImpl<_$_TimeModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TimeModelToJson(
      this,
    );
  }
}

abstract class _TimeModel implements TimeModel {
  const factory _TimeModel(
      {final CurrencyType code,
      final String updated,
      final String updatedISO,
      @JsonKey(name: 'updateduk') final String updateDuk}) = _$_TimeModel;

  factory _TimeModel.fromJson(Map<String, dynamic> json) =
      _$_TimeModel.fromJson;

  @override
  CurrencyType get code;
  @override
  String get updated;
  @override
  String get updatedISO;
  @override
  @JsonKey(name: 'updateduk')
  String get updateDuk;
  @override
  @JsonKey(ignore: true)
  _$$_TimeModelCopyWith<_$_TimeModel> get copyWith =>
      throw _privateConstructorUsedError;
}
