// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'time_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TimeModel _$$_TimeModelFromJson(Map<String, dynamic> json) => _$_TimeModel(
      code: $enumDecodeNullable(_$CurrencyTypeEnumMap, json['code']) ??
          CurrencyType.USD,
      updated: json['updated'] as String? ?? '',
      updatedISO: json['updatedISO'] as String? ?? '',
      updateDuk: json['updateduk'] as String? ?? '',
    );

Map<String, dynamic> _$$_TimeModelToJson(_$_TimeModel instance) =>
    <String, dynamic>{
      'code': _$CurrencyTypeEnumMap[instance.code]!,
      'updated': instance.updated,
      'updatedISO': instance.updatedISO,
      'updateduk': instance.updateDuk,
    };

const _$CurrencyTypeEnumMap = {
  CurrencyType.USD: 'USD',
  CurrencyType.GBP: 'GBP',
  CurrencyType.EUR: 'EUR',
};
