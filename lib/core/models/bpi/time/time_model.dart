import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:testing_project/core/enum/export.dart';

part 'time_model.freezed.dart';

part 'time_model.g.dart';

@freezed
class TimeModel with _$TimeModel {
  const factory TimeModel({
    @Default(CurrencyType.USD) CurrencyType code,
    @Default('') String updated,
    @Default('') String updatedISO,
    @Default('') @JsonKey(name: 'updateduk') String updateDuk,
  }) = _TimeModel;

  factory TimeModel.fromJson(Map<String, Object?> json) => _$TimeModelFromJson(json);
}
