import 'package:freezed_annotation/freezed_annotation.dart';
import 'currency_bpi/currency_bpi.dart';
import 'time/time_model.dart';

part 'bpi.freezed.dart';

part 'bpi.g.dart';

@freezed
class Bpi with _$Bpi {
  const factory Bpi({
    @Default(TimeModel()) TimeModel time,
    @Default('') String disclaimer,
    @Default('') String chartName,
    @Default(CurrencyBpi()) CurrencyBpi bpi,
  }) = _Bpi;

  factory Bpi.fromJson(Map<String, Object?> json) => _$BpiFromJson(json);
}
