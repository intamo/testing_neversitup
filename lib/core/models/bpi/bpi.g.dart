// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bpi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Bpi _$$_BpiFromJson(Map<String, dynamic> json) => _$_Bpi(
      time: json['time'] == null
          ? const TimeModel()
          : TimeModel.fromJson(json['time'] as Map<String, dynamic>),
      disclaimer: json['disclaimer'] as String? ?? '',
      chartName: json['chartName'] as String? ?? '',
      bpi: json['bpi'] == null
          ? const CurrencyBpi()
          : CurrencyBpi.fromJson(json['bpi'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_BpiToJson(_$_Bpi instance) => <String, dynamic>{
      'time': instance.time,
      'disclaimer': instance.disclaimer,
      'chartName': instance.chartName,
      'bpi': instance.bpi,
    };
