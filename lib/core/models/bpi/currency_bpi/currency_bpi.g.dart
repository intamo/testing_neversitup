// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_bpi.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CurrencyBpi _$$_CurrencyBpiFromJson(Map<String, dynamic> json) =>
    _$_CurrencyBpi(
      usd: json['USD'] == null
          ? const CurrencyModel()
          : CurrencyModel.fromJson(json['USD'] as Map<String, dynamic>),
      gbp: json['GBP'] == null
          ? const CurrencyModel()
          : CurrencyModel.fromJson(json['GBP'] as Map<String, dynamic>),
      eur: json['EUR'] == null
          ? const CurrencyModel()
          : CurrencyModel.fromJson(json['EUR'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CurrencyBpiToJson(_$_CurrencyBpi instance) =>
    <String, dynamic>{
      'USD': instance.usd,
      'GBP': instance.gbp,
      'EUR': instance.eur,
    };
