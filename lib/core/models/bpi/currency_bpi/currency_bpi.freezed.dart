// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currency_bpi.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CurrencyBpi _$CurrencyBpiFromJson(Map<String, dynamic> json) {
  return _CurrencyBpi.fromJson(json);
}

/// @nodoc
mixin _$CurrencyBpi {
  @JsonKey(name: 'USD')
  CurrencyModel get usd => throw _privateConstructorUsedError;
  @JsonKey(name: 'GBP')
  CurrencyModel get gbp => throw _privateConstructorUsedError;
  @JsonKey(name: 'EUR')
  CurrencyModel get eur => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrencyBpiCopyWith<CurrencyBpi> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyBpiCopyWith<$Res> {
  factory $CurrencyBpiCopyWith(
          CurrencyBpi value, $Res Function(CurrencyBpi) then) =
      _$CurrencyBpiCopyWithImpl<$Res, CurrencyBpi>;
  @useResult
  $Res call(
      {@JsonKey(name: 'USD') CurrencyModel usd,
      @JsonKey(name: 'GBP') CurrencyModel gbp,
      @JsonKey(name: 'EUR') CurrencyModel eur});

  $CurrencyModelCopyWith<$Res> get usd;
  $CurrencyModelCopyWith<$Res> get gbp;
  $CurrencyModelCopyWith<$Res> get eur;
}

/// @nodoc
class _$CurrencyBpiCopyWithImpl<$Res, $Val extends CurrencyBpi>
    implements $CurrencyBpiCopyWith<$Res> {
  _$CurrencyBpiCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? usd = null,
    Object? gbp = null,
    Object? eur = null,
  }) {
    return _then(_value.copyWith(
      usd: null == usd
          ? _value.usd
          : usd // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
      gbp: null == gbp
          ? _value.gbp
          : gbp // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
      eur: null == eur
          ? _value.eur
          : eur // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $CurrencyModelCopyWith<$Res> get usd {
    return $CurrencyModelCopyWith<$Res>(_value.usd, (value) {
      return _then(_value.copyWith(usd: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $CurrencyModelCopyWith<$Res> get gbp {
    return $CurrencyModelCopyWith<$Res>(_value.gbp, (value) {
      return _then(_value.copyWith(gbp: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $CurrencyModelCopyWith<$Res> get eur {
    return $CurrencyModelCopyWith<$Res>(_value.eur, (value) {
      return _then(_value.copyWith(eur: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_CurrencyBpiCopyWith<$Res>
    implements $CurrencyBpiCopyWith<$Res> {
  factory _$$_CurrencyBpiCopyWith(
          _$_CurrencyBpi value, $Res Function(_$_CurrencyBpi) then) =
      __$$_CurrencyBpiCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'USD') CurrencyModel usd,
      @JsonKey(name: 'GBP') CurrencyModel gbp,
      @JsonKey(name: 'EUR') CurrencyModel eur});

  @override
  $CurrencyModelCopyWith<$Res> get usd;
  @override
  $CurrencyModelCopyWith<$Res> get gbp;
  @override
  $CurrencyModelCopyWith<$Res> get eur;
}

/// @nodoc
class __$$_CurrencyBpiCopyWithImpl<$Res>
    extends _$CurrencyBpiCopyWithImpl<$Res, _$_CurrencyBpi>
    implements _$$_CurrencyBpiCopyWith<$Res> {
  __$$_CurrencyBpiCopyWithImpl(
      _$_CurrencyBpi _value, $Res Function(_$_CurrencyBpi) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? usd = null,
    Object? gbp = null,
    Object? eur = null,
  }) {
    return _then(_$_CurrencyBpi(
      usd: null == usd
          ? _value.usd
          : usd // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
      gbp: null == gbp
          ? _value.gbp
          : gbp // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
      eur: null == eur
          ? _value.eur
          : eur // ignore: cast_nullable_to_non_nullable
              as CurrencyModel,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CurrencyBpi implements _CurrencyBpi {
  const _$_CurrencyBpi(
      {@JsonKey(name: 'USD') this.usd = const CurrencyModel(),
      @JsonKey(name: 'GBP') this.gbp = const CurrencyModel(),
      @JsonKey(name: 'EUR') this.eur = const CurrencyModel()});

  factory _$_CurrencyBpi.fromJson(Map<String, dynamic> json) =>
      _$$_CurrencyBpiFromJson(json);

  @override
  @JsonKey(name: 'USD')
  final CurrencyModel usd;
  @override
  @JsonKey(name: 'GBP')
  final CurrencyModel gbp;
  @override
  @JsonKey(name: 'EUR')
  final CurrencyModel eur;

  @override
  String toString() {
    return 'CurrencyBpi(usd: $usd, gbp: $gbp, eur: $eur)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CurrencyBpi &&
            (identical(other.usd, usd) || other.usd == usd) &&
            (identical(other.gbp, gbp) || other.gbp == gbp) &&
            (identical(other.eur, eur) || other.eur == eur));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, usd, gbp, eur);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CurrencyBpiCopyWith<_$_CurrencyBpi> get copyWith =>
      __$$_CurrencyBpiCopyWithImpl<_$_CurrencyBpi>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrencyBpiToJson(
      this,
    );
  }
}

abstract class _CurrencyBpi implements CurrencyBpi {
  const factory _CurrencyBpi(
      {@JsonKey(name: 'USD') final CurrencyModel usd,
      @JsonKey(name: 'GBP') final CurrencyModel gbp,
      @JsonKey(name: 'EUR') final CurrencyModel eur}) = _$_CurrencyBpi;

  factory _CurrencyBpi.fromJson(Map<String, dynamic> json) =
      _$_CurrencyBpi.fromJson;

  @override
  @JsonKey(name: 'USD')
  CurrencyModel get usd;
  @override
  @JsonKey(name: 'GBP')
  CurrencyModel get gbp;
  @override
  @JsonKey(name: 'EUR')
  CurrencyModel get eur;
  @override
  @JsonKey(ignore: true)
  _$$_CurrencyBpiCopyWith<_$_CurrencyBpi> get copyWith =>
      throw _privateConstructorUsedError;
}
