import 'package:freezed_annotation/freezed_annotation.dart';

import '../currency/currency_model.dart';

part 'currency_bpi.freezed.dart';

part 'currency_bpi.g.dart';

@freezed
class CurrencyBpi with _$CurrencyBpi {
  const factory CurrencyBpi({
    @Default(CurrencyModel())  @JsonKey(name: 'USD') CurrencyModel usd,
    @Default(CurrencyModel())  @JsonKey(name: 'GBP') CurrencyModel gbp,
    @Default(CurrencyModel())  @JsonKey(name: 'EUR') CurrencyModel eur,
  }) = _CurrencyBpi;

  factory CurrencyBpi.fromJson(Map<String, Object?> json) => _$CurrencyBpiFromJson(json);
}
