import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:testing_project/core/enum/export.dart';

part 'currency_model.freezed.dart';

part 'currency_model.g.dart';

@freezed
class CurrencyModel with _$CurrencyModel {
  const factory CurrencyModel({
    @Default(CurrencyType.USD) CurrencyType code,
    @Default('') String symbol,
    @Default('0.0') String rate,
    @Default('') String description,
    @Default(0.0) @JsonKey(name: 'rate_float') double rateFloat,
  }) = _CurrencyModel;

  factory CurrencyModel.fromJson(Map<String, Object?> json) => _$CurrencyModelFromJson(json);
}
