// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CurrencyModel _$$_CurrencyModelFromJson(Map<String, dynamic> json) =>
    _$_CurrencyModel(
      code: $enumDecodeNullable(_$CurrencyTypeEnumMap, json['code']) ??
          CurrencyType.USD,
      symbol: json['symbol'] as String? ?? '',
      rate: json['rate'] as String? ?? '0.0',
      description: json['description'] as String? ?? '',
      rateFloat: (json['rate_float'] as num?)?.toDouble() ?? 0.0,
    );

Map<String, dynamic> _$$_CurrencyModelToJson(_$_CurrencyModel instance) =>
    <String, dynamic>{
      'code': _$CurrencyTypeEnumMap[instance.code]!,
      'symbol': instance.symbol,
      'rate': instance.rate,
      'description': instance.description,
      'rate_float': instance.rateFloat,
    };

const _$CurrencyTypeEnumMap = {
  CurrencyType.USD: 'USD',
  CurrencyType.GBP: 'GBP',
  CurrencyType.EUR: 'EUR',
};
