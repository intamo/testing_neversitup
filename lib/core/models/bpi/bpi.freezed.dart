// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'bpi.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Bpi _$BpiFromJson(Map<String, dynamic> json) {
  return _Bpi.fromJson(json);
}

/// @nodoc
mixin _$Bpi {
  TimeModel get time => throw _privateConstructorUsedError;
  String get disclaimer => throw _privateConstructorUsedError;
  String get chartName => throw _privateConstructorUsedError;
  CurrencyBpi get bpi => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BpiCopyWith<Bpi> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BpiCopyWith<$Res> {
  factory $BpiCopyWith(Bpi value, $Res Function(Bpi) then) =
      _$BpiCopyWithImpl<$Res, Bpi>;
  @useResult
  $Res call(
      {TimeModel time, String disclaimer, String chartName, CurrencyBpi bpi});

  $TimeModelCopyWith<$Res> get time;
  $CurrencyBpiCopyWith<$Res> get bpi;
}

/// @nodoc
class _$BpiCopyWithImpl<$Res, $Val extends Bpi> implements $BpiCopyWith<$Res> {
  _$BpiCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? time = null,
    Object? disclaimer = null,
    Object? chartName = null,
    Object? bpi = null,
  }) {
    return _then(_value.copyWith(
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as TimeModel,
      disclaimer: null == disclaimer
          ? _value.disclaimer
          : disclaimer // ignore: cast_nullable_to_non_nullable
              as String,
      chartName: null == chartName
          ? _value.chartName
          : chartName // ignore: cast_nullable_to_non_nullable
              as String,
      bpi: null == bpi
          ? _value.bpi
          : bpi // ignore: cast_nullable_to_non_nullable
              as CurrencyBpi,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $TimeModelCopyWith<$Res> get time {
    return $TimeModelCopyWith<$Res>(_value.time, (value) {
      return _then(_value.copyWith(time: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $CurrencyBpiCopyWith<$Res> get bpi {
    return $CurrencyBpiCopyWith<$Res>(_value.bpi, (value) {
      return _then(_value.copyWith(bpi: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_BpiCopyWith<$Res> implements $BpiCopyWith<$Res> {
  factory _$$_BpiCopyWith(_$_Bpi value, $Res Function(_$_Bpi) then) =
      __$$_BpiCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {TimeModel time, String disclaimer, String chartName, CurrencyBpi bpi});

  @override
  $TimeModelCopyWith<$Res> get time;
  @override
  $CurrencyBpiCopyWith<$Res> get bpi;
}

/// @nodoc
class __$$_BpiCopyWithImpl<$Res> extends _$BpiCopyWithImpl<$Res, _$_Bpi>
    implements _$$_BpiCopyWith<$Res> {
  __$$_BpiCopyWithImpl(_$_Bpi _value, $Res Function(_$_Bpi) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? time = null,
    Object? disclaimer = null,
    Object? chartName = null,
    Object? bpi = null,
  }) {
    return _then(_$_Bpi(
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as TimeModel,
      disclaimer: null == disclaimer
          ? _value.disclaimer
          : disclaimer // ignore: cast_nullable_to_non_nullable
              as String,
      chartName: null == chartName
          ? _value.chartName
          : chartName // ignore: cast_nullable_to_non_nullable
              as String,
      bpi: null == bpi
          ? _value.bpi
          : bpi // ignore: cast_nullable_to_non_nullable
              as CurrencyBpi,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Bpi implements _Bpi {
  const _$_Bpi(
      {this.time = const TimeModel(),
      this.disclaimer = '',
      this.chartName = '',
      this.bpi = const CurrencyBpi()});

  factory _$_Bpi.fromJson(Map<String, dynamic> json) => _$$_BpiFromJson(json);

  @override
  @JsonKey()
  final TimeModel time;
  @override
  @JsonKey()
  final String disclaimer;
  @override
  @JsonKey()
  final String chartName;
  @override
  @JsonKey()
  final CurrencyBpi bpi;

  @override
  String toString() {
    return 'Bpi(time: $time, disclaimer: $disclaimer, chartName: $chartName, bpi: $bpi)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Bpi &&
            (identical(other.time, time) || other.time == time) &&
            (identical(other.disclaimer, disclaimer) ||
                other.disclaimer == disclaimer) &&
            (identical(other.chartName, chartName) ||
                other.chartName == chartName) &&
            (identical(other.bpi, bpi) || other.bpi == bpi));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, time, disclaimer, chartName, bpi);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BpiCopyWith<_$_Bpi> get copyWith =>
      __$$_BpiCopyWithImpl<_$_Bpi>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BpiToJson(
      this,
    );
  }
}

abstract class _Bpi implements Bpi {
  const factory _Bpi(
      {final TimeModel time,
      final String disclaimer,
      final String chartName,
      final CurrencyBpi bpi}) = _$_Bpi;

  factory _Bpi.fromJson(Map<String, dynamic> json) = _$_Bpi.fromJson;

  @override
  TimeModel get time;
  @override
  String get disclaimer;
  @override
  String get chartName;
  @override
  CurrencyBpi get bpi;
  @override
  @JsonKey(ignore: true)
  _$$_BpiCopyWith<_$_Bpi> get copyWith => throw _privateConstructorUsedError;
}
