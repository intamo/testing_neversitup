import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'coin_desk/coin_desk_dio_interceptors.dart';

class DioService {
  late Dio coinDeskClientService;

  static final DioService _dioService = DioService._internal();

  factory DioService() {
    return _dioService;
  }

  DioService._internal() {
    final coinDeskClientDioOptions = BaseOptions(
      baseUrl: dotenv.env['COIN_DESK_API']!,
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    coinDeskClientService = Dio(coinDeskClientDioOptions);

    coinDeskClientService.interceptors.add(CoinDeskDioInterceptors());
  }
}
