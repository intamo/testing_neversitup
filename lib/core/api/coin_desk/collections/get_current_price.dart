import 'dart:convert';

import 'package:testing_project/core/api/dio_service.dart';
import 'package:testing_project/core/models/bpi/bpi.dart';

Future<Bpi> getBpi() async {
  final result = await DioService().coinDeskClientService.get('');
  if (result.data != null && result.statusCode == 200) {
    print(result.data);
    final Bpi bpi = Bpi.fromJson(jsonDecode(result.data));

    return bpi;
  }
  return const Bpi();
}
