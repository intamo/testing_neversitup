import 'package:intl/intl.dart';

extension DateExtension on DateTime {
  /// This is DateTime extension
  /// Example DateTime.this function

  /// Return month year
  /// Example July 2022
  String get toMonthAndYear => DateFormat('MMMM yyyy').format(toLocal());

  /// Return full date => 1 January 2022
  String get toFullDate => DateFormat('dd MMMM y').format(toLocal());

  /// Return full date => 20/04/2022
  String get toddMMy => DateFormat('dd/MM/y').format(toLocal());

  /// Return full date with time => 20 July 2022, 18:30
  String get toFullDateTime => DateFormat('dd MMMM y, HH:mm:ss').format(toLocal());

  /// Return full date with time => 20 July 2022
  String get toFullDateNoTime => DateFormat('dd MMM y').format(toLocal());

  /// To return hour => 00.00
  String get toHours => DateFormat('Hm').format(toLocal());

  /// Return 18:30 AM
  String get toTime => DateFormat('HH:mm a').format(toLocal());

  /// Return HH:mm
  String get toHHmm => DateFormat('HH:mm').format(toLocal());

  /// Get datetime with out time
  DateTime get toDayWithOutTime => DateTime(year, month, day);
}
