import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'bonus_state.dart';

class BonusCubit extends Cubit<BonusState> {
  BonusCubit() : super(const BonusState());

  void onGenerateFibonacciNumbers(int range) {
    List<int> result = [];

    for (int i = 0; i < range; i++) {
      result.add(fibonacci(i));
    }

    emit(state.copyWith(fibonacciNumbers: [...result]));
  }

  void onGeneratePrimeNumbers(int range) {
    List<int> result = [];

    for (int i = 0; i < range; i++) {
      if (isPrimeNumber(i)) {
        result.add(i);
      }
    }

    emit(state.copyWith(primeNumbers: [...result]));
  }

  int fibonacci(int n) {
    // The first two numbers in the sequence are 0 and 1
    if (n == 0 || n == 1) {
      return n;
    }

    // The nth number in the sequence is the sum of the (n - 1)th and (n - 2)th numbers
    return fibonacci(n - 1) + fibonacci(n - 2);
  }

  bool isPrimeNumber(int num) {
    if (num == 1 || num == 0) {
      return false;
    }

    for (int i = 2; i <= num / i; ++i) {
      if (num % i == 0) {
        return false;
      }
    }
    return true;
  }
}
