part of 'bonus_cubit.dart';

class BonusState extends Equatable {
  final List<int> primeNumbers;
  final List<int> fibonacciNumbers;

  const BonusState({
    this.primeNumbers = const [],
    this.fibonacciNumbers = const [],
  });

  BonusState copyWith({List<int>? primeNumbers, List<int>? fibonacciNumbers}) {
    return BonusState(
      primeNumbers: primeNumbers ?? this.primeNumbers,
      fibonacciNumbers: fibonacciNumbers ?? this.fibonacciNumbers,
    );
  }

  @override
  List<Object?> get props => [primeNumbers, fibonacciNumbers];
}
