import 'package:flutter/material.dart';

import 'screen/bonus_screen.dart';

Map<String, Widget Function(BuildContext)> bonusRoutes = {
  "/bonus": (context) {
    // final args =
    //     ModalRoute.of(context)!.settings.arguments as AnimeListArguments;
    return const BonusScreen();
  },
};
