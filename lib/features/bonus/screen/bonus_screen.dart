import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testing_project/features/bonus/cubit/bonus_cubit.dart';
import 'bonus_layout.dart';

class BonusScreen extends StatelessWidget {
  const BonusScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BonusCubit>(
      create: (context) => BonusCubit(),
      child: const BonusLayout(),
    );
  }
}
