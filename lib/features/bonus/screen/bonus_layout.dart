import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testing_project/core/widgets/scaffold/app_scaffold.dart';
import 'package:testing_project/features/bonus/cubit/bonus_cubit.dart';
import 'package:testing_project/features/bonus/widgets/generate_form.dart';

class BonusLayout extends StatelessWidget {
  const BonusLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: BlocBuilder<BonusCubit, BonusState>(
          builder: (context, state) {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  GeneratorForm(
                    title: 'Fibonacci number',
                    result: state.fibonacciNumbers,
                    onFieldSubmitted: (text) {
                      if (text != '') {
                        context.read<BonusCubit>().onGenerateFibonacciNumbers(int.parse(text));
                      }
                    },
                  ),
                  const SizedBox(height: 32.0),
                  const Divider(
                    height: 1.0,
                    thickness: 1.0,
                    color: Colors.white70,
                  ),
                  const SizedBox(height: 32.0),
                  GeneratorForm(
                    title: 'Prime number',
                    result: state.primeNumbers,
                    onFieldSubmitted: (text) {
                      if (text != '') {
                        context.read<BonusCubit>().onGeneratePrimeNumbers(int.parse(text));
                      }
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

