import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:testing_project/core/widgets/button/custom_button.dart';

class GeneratorForm extends StatefulWidget {
  const GeneratorForm({
    Key? key,
    required this.title,
    required this.result,
    required this.onFieldSubmitted,
  }) : super(key: key);

  final String title;
  final List<int> result;
  final Function(String) onFieldSubmitted;

  @override
  State<GeneratorForm> createState() => _GeneratorFormState();
}

class _GeneratorFormState extends State<GeneratorForm> {
  final _formKey = GlobalKey<FormBuilderState>();

  void _onFieldSubmitted() {
    _formKey.currentState?.save();
    widget.onFieldSubmitted(_formKey.currentState?.fields['field']?.value ?? '');
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text(
            widget.title,
            style: const TextStyle(fontSize: 18.0, color: Colors.white70),
          ),
          const SizedBox(height: 8.0),
          Row(
            children: [
              Expanded(
                child: _FormField(
                  name: 'field',
                  onFieldSubmitted: (value) => _onFieldSubmitted(),
                ),
              ),
              const SizedBox(width: 8.0),
              CustomButton(
                label: 'Generate',
                onTap: () => _onFieldSubmitted(),
              ),
            ],
          ),
          const SizedBox(height: 8.0),
          const Text(
            '*** Beware app crashes  !! If enter high number ***',
            style: TextStyle(fontSize: 16.0, color: Colors.red),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 8.0),
          const Text(
            'Result',
            style: TextStyle(fontSize: 16.0, color: Colors.white70),
          ),
          const SizedBox(height: 8.0),
          Container(
            padding: const EdgeInsets.all(16.0),
            decoration: BoxDecoration(
              color: const Color(0xFF3B3A42),
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Text(
              widget.result.toString(),
              style: const TextStyle(
                fontSize: 16.0,
                color: Colors.white70,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }
}

class _FormField extends StatelessWidget {
  const _FormField({
    Key? key,
    required this.name,
    required this.onFieldSubmitted,
  }) : super(key: key);

  final String name;

  final Function(String) onFieldSubmitted;

  @override
  Widget build(BuildContext context) {
    return FormBuilderField<String?>(
      name: name,
      builder: (FormFieldState field) {
        return TextFormField(
          enabled: true,
          maxLines: 1,
          onChanged: (value) => field.didChange(value),
          onFieldSubmitted: onFieldSubmitted,
          textInputAction: TextInputAction.done,
          inputFormatters: [
            FilteringTextInputFormatter.digitsOnly,
          ],
          keyboardType: TextInputType.number,
          decoration: const InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white70,
                width: 1.5,
              ),
              borderRadius: BorderRadius.all(Radius.circular(8.0)),
            ),
            contentPadding: EdgeInsets.all(16.0),
            isDense: true,
            hintText: 'Enter int number',
            hintStyle: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w500,
              color: Colors.white70,
            ),
          ),
          style: const TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w500,
            color: Colors.white,
          ),
        );
      },
    );
  }
}
