import 'package:flutter/material.dart';
import 'package:testing_project/core/widgets/button/custom_button.dart';
import 'package:testing_project/core/widgets/scaffold/app_scaffold.dart';
import 'package:testing_project/features/home/widgets/question_container.dart';
import 'package:testing_project/main.dart';

class HomeLayout extends StatelessWidget {
  const HomeLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text(
              'Testing Project',
              style: TextStyle(
                color: Colors.white,
                fontSize: 24.0,
                fontWeight: FontWeight.w500,
              ),
            ),
            const SizedBox(height: 16.0),
            const QuestionContainer(
              title: 'โจทย์',
              detail: '''สร้าง Application โดยที่มีฟีเจอร์ดังนี้
  a. แสดงผล currency ของ BTC ของทั้ง 3 ราคา (USD, GBP และ EUR) จาก API
  b. เรทราคา จะต้องมีการ auto update ทุก ๆ 1 นาที
  c. สามารถดูราคาย้อนหลังได้ (โดยจะต้องบันทึกข้อมูลจากข้อ a.)
  d. มี Input field ให้เลือก currency 3 แบบตามข้อ a. จากนั้น สามารถกรอกจำนวนของ currency นั้น ๆ เพื่อแปลงค่าเป็น BTC ได้''',
            ),
            const SizedBox(height: 8.0),
            CustomButton(
              onTap: () => navigatorKey.currentState?.pushNamed('/currency'),
              label: 'GO',
            ),
            const SizedBox(height: 32.0),
            const QuestionContainer(
              title: 'โบนัส',
              detail: '''เขียน program generate ตัวเลข Fibonacci (0, 1, 1, 2, 3, 5, 8, 13, …)
เขียน program generate จำนวนเฉพาะ (2, 3, 5, 7, 11, 13, 17, 19, …)''',
            ),
            const SizedBox(height: 8.0),
            CustomButton(
              onTap: () => navigatorKey.currentState?.pushNamed('/bonus'),
              label: 'GO',
            ),
          ],
        ),
      ),
    );
  }
}
