import 'package:flutter/material.dart';

import 'screen/home_screen.dart';

Map<String, Widget Function(BuildContext)> homeRoutes = {
  "/": (context) {
    // final args =
    //     ModalRoute.of(context)!.settings.arguments as AnimeListArguments;
    return const HomeScreen();
  },
};

// class ExampleArguments {
//   ExampleArguments();
// }
