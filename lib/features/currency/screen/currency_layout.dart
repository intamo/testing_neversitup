import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testing_project/core/enum/export.dart';
import 'package:testing_project/core/extension/export.dart';
import 'package:testing_project/core/widgets/scaffold/app_scaffold.dart';
import 'package:testing_project/features/currency/cubit/currency_cubit.dart';

import '../widgets/balance_banner.dart';
import '../widgets/currency_card.dart';
import '../widgets/history_layout.dart';

class CurrencyLayout extends StatelessWidget {
  const CurrencyLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: AppScaffold(
        child: BlocBuilder<CurrencyCubit, CurrencyState>(
          builder: (context, state) {
            if (state.status.isLoading) {
              return const Center(
                child: CircularProgressIndicator(color: Colors.white),
              );
            }
            if (state.status.isError) {
              return Center(
                child: Text(state.statusMessage),
              );
            }
            return SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    BalanceBanner(
                      title: state.bpis.last.chartName,
                      symbol: 'BTC',
                      balance: state.getBalanceBTC,
                    ),
                    const SizedBox(height: 16.0),
                    BalanceBanner(
                      title: state.type.name,
                      symbol: state.type.name,
                      balance: state.balance,
                      enabled: true,
                      onChanged: (String text) {
                        if (text != '') {
                          context
                              .read<CurrencyCubit>()
                              .onUpdateBalance(double.parse(text.replaceAll(',', '')));
                        }
                      },
                    ),
                    const SizedBox(height: 16.0),

                    /// Update time
                    Text(
                      'Update at ${DateTime.parse(state.bpis.last.time.updatedISO).toFullDateTime}',
                      style: const TextStyle(fontSize: 14.0, color: Colors.white70),
                    ),
                    const SizedBox(height: 8.0),

                    /// Current price
                    CurrencyCard(
                      type: 'USD',
                      currency: state.bpis.last.bpi.usd,
                      lastCurrency: state.lastBpi.bpi.usd,
                      onSelected: state.type == CurrencyType.USD,
                      onTap: () {
                        context.read<CurrencyCubit>().onCurrencyTypeChange(CurrencyType.USD);
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    const SizedBox(height: 8.0),
                    CurrencyCard(
                      type: 'GBP',
                      currency: state.bpis.last.bpi.gbp,
                      lastCurrency: state.lastBpi.bpi.gbp,
                      onSelected: state.type == CurrencyType.GBP,
                      onTap: () {
                        context.read<CurrencyCubit>().onCurrencyTypeChange(CurrencyType.GBP);
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    const SizedBox(height: 8.0),
                    CurrencyCard(
                      type: 'EUR',
                      currency: state.bpis.last.bpi.eur,
                      lastCurrency: state.lastBpi.bpi.eur,
                      onSelected: state.type == CurrencyType.EUR,
                      onTap: () {
                        context.read<CurrencyCubit>().onCurrencyTypeChange(CurrencyType.EUR);
                        FocusScope.of(context).unfocus();
                      },
                    ),
                    const SizedBox(height: 16.0),

                    /// History
                    Text(
                      'History ${state.type.name}',
                      style: const TextStyle(fontSize: 14.0, color: Colors.white70),
                    ),
                    const SizedBox(height: 8.0),
                    HistoryLayout(
                      bpis: state.bpis,
                      type: state.type,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
