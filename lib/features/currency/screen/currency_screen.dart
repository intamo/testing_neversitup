import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testing_project/features/currency/cubit/currency_cubit.dart';

import 'currency_layout.dart';

class CurrencyScreen extends StatelessWidget {
  const CurrencyScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<CurrencyCubit>(
      lazy: false,
      create: (context) => CurrencyCubit()..onFetch(),
      child: const CurrencyLayout(),
    );
  }
}
