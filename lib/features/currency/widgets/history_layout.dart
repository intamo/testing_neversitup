import 'package:flutter/material.dart';
import 'package:testing_project/core/enum/export.dart';
import 'package:testing_project/core/extension/export.dart';
import 'package:testing_project/core/models/bpi/bpi.dart';
import 'package:testing_project/core/models/bpi/currency_bpi/currency_bpi.dart';

class HistoryLayout extends StatelessWidget {
  const HistoryLayout({
    Key? key,
    required this.bpis,
    this.type = CurrencyType.USD,
  }) : super(key: key);

  final List<Bpi> bpis;
  final CurrencyType type;

  String getRateByType(CurrencyBpi bpi) {
    switch (type) {
      case CurrencyType.USD:
        return bpi.usd.rate;
      case CurrencyType.GBP:
        return bpi.gbp.rate;
      case CurrencyType.EUR:
        return bpi.eur.rate;
      default:
        return bpi.usd.rate;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16.0),
      decoration:
          BoxDecoration(color: const Color(0xFF3B3A42), borderRadius: BorderRadius.circular(8.0)),
      child: Column(
        children: [
          const _TableRow(
            children: [
              Text(
                'Date',
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
              Text(
                'Rate',
                style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          const SizedBox(height: 16.0),
          const Divider(
            height: 1.0,
            thickness: 1.0,
            color: Colors.white70,
          ),
          const SizedBox(height: 12.0),
          ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            reverse: true,
            itemCount: bpis.length,
            itemBuilder: (context, index) {
              Bpi bpi = bpis[index];

              return Column(
                children: [
                  _TableRow(
                    children: [
                      Text(
                        DateTime.parse(bpi.time.updatedISO).toFullDateTime,
                        style: const TextStyle(
                          fontSize: 12.0,
                          color: Colors.white70,
                        ),
                      ),
                      Text(
                        getRateByType(bpi.bpi),
                        style: const TextStyle(
                          fontSize: 12.0,
                          color: Colors.white70,
                        ),
                      ),
                    ],
                  ),
                ],
              );
            },
            separatorBuilder: (context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: Divider(
                  height: 1.0,
                  thickness: 1.0,
                  color: Colors.white70,
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

class _TableRow extends StatelessWidget {
  const _TableRow({
    Key? key,
    required this.children,
  }) : super(key: key);

  /// Limit 2 position
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(flex: 3, child: children[0]),
        Expanded(flex: 2, child: children[1]),
      ],
    );
  }
}
