import 'package:flutter/material.dart';
import 'package:testing_project/core/models/bpi/currency/currency_model.dart';

class CurrencyCard extends StatelessWidget {
  const CurrencyCard({
    Key? key,
    required this.type,
    required this.currency,
    required this.lastCurrency,
    this.onSelected = false,
    required this.onTap,
  }) : super(key: key);

  final String type;
  final CurrencyModel currency;
  final CurrencyModel lastCurrency;
  final bool onSelected;
  final Function() onTap;

  double get percentRate {
    double rate = currency.rateFloat - lastCurrency.rateFloat;
    if (lastCurrency.rateFloat != 0) {
      return (rate / lastCurrency.rateFloat) * 100;
    }
    return 0.0;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(16.0),
        decoration: BoxDecoration(
          color: const Color(0xFF3B3A42),
          border: onSelected ? Border.all(width: 1.0, color: Colors.white70) : null,
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  type,
                  style: const TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.white,
                  ),
                ),
                const Icon(
                  Icons.arrow_forward,
                  size: 20.0,
                  color: Colors.white,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${percentRate >= 0 ? '+' : '-'} ${percentRate.abs().toStringAsFixed(4)} %',
                      style: TextStyle(
                        fontSize: 12.0,
                        color: percentRate >= 0 ? Colors.green : Colors.red,
                      ),
                    ),
                    const SizedBox(height: 4.0),
                    Text(
                      'Rate: ${currency.rate}',
                      style: const TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(height: 4.0),
                    Text(
                      'Last rate: ${lastCurrency.rate}',
                      style: const TextStyle(
                        fontSize: 14.0,
                        fontWeight: FontWeight.w300,
                        color: Colors.white70,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
