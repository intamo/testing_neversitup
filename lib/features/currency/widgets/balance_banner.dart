import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:testing_project/core/extension/export.dart';

class BalanceBanner extends StatefulWidget {
  const BalanceBanner({
    Key? key,
    required this.title,
    required this.balance,
    required this.symbol,
    this.enabled = false,
    this.onChanged,
  }) : super(key: key);

  final String title;
  final double balance;
  final String symbol;

  final bool enabled;
  final Function(String)? onChanged;

  @override
  State<BalanceBanner> createState() => _BalanceBannerState();
}

class _BalanceBannerState extends State<BalanceBanner> {
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          'Balance ${widget.title}',
          style: const TextStyle(fontSize: 14.0, color: Colors.white70),
        ),
        const SizedBox(height: 8.0),
        Row(
          children: [
            Expanded(
              child: !widget.enabled
                  ? Text(
                      widget.balance.toPriceFormat,
                      style: const TextStyle(
                        fontSize: 28.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                      overflow: TextOverflow.ellipsis,
                    )
                  : _TextField(
                      controller: _controller,
                      onChanged: widget.onChanged,
                    ),
            ),
            const SizedBox(width: 16.0),
            Text(
              widget.symbol,
              style: const TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _TextField extends StatelessWidget {
  const _TextField({
    Key? key,
    required this.controller,
    this.onChanged,
  }) : super(key: key);

  final TextEditingController controller;
  final Function(String)? onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: true,
      maxLines: 1,
      onChanged: onChanged,
      controller: controller,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp(r'(^-?\d*\.?\d*)')),
      ],
      onFieldSubmitted: (String text) {
        if (text != '') {
          controller.value =
              TextEditingValue(text: double.parse(text.replaceAll(',', '')).toPriceFormat);
        }
      },
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white70,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white70,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white70,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white70,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white70,
            width: 1.5,
          ),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        contentPadding: EdgeInsets.all(8.0),
        hintText: 'Enter balance',
        hintStyle: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.w500,
          color: Colors.white70,
        ),
        isDense: true,
      ),
      style: const TextStyle(
        fontSize: 28.0,
        fontWeight: FontWeight.w500,
        color: Colors.white,
      ),
    );
  }
}
