import 'package:flutter/material.dart';

import 'screen/currency_screen.dart';

Map<String, Widget Function(BuildContext)> currencyRoutes = {
  "/currency": (context) {
    // final args =
    //     ModalRoute.of(context)!.settings.arguments as AnimeListArguments;
    return const CurrencyScreen();
  },
};

// class ExampleArguments {
//   ExampleArguments();
// }
