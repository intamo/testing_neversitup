part of 'currency_cubit.dart';

class CurrencyState extends Equatable {
  final List<Bpi> bpis;
  final double balance;
  final CurrencyType type;
  final NetWorkStatus status;
  final NetWorkStatus statusAuto;
  final String statusMessage;

  const CurrencyState({
    this.bpis = const [],
    this.balance = 0.0,
    this.type = CurrencyType.USD,
    this.status = NetWorkStatus.initial,
    this.statusAuto = NetWorkStatus.initial,
    this.statusMessage = 'Initial',
  });

  const CurrencyState.init({
    this.bpis = const [],
    this.balance = 0.0,
    this.type = CurrencyType.USD,
    this.status = NetWorkStatus.initial,
    this.statusAuto = NetWorkStatus.initial,
    this.statusMessage = 'Initial',
  });

  @override
  List<Object?> get props => [bpis, balance, type, status, statusAuto, statusMessage];

  CurrencyState copyWith({
    List<Bpi>? bpis,
    double? balance,
    CurrencyType? type,
    NetWorkStatus? status,
    NetWorkStatus? statusAuto,
    String? statusMessage,
  }) {
    return CurrencyState(
      bpis: bpis ?? this.bpis,
      balance: balance ?? this.balance,
      type: type ?? this.type,
      status: status ?? this.status,
      statusAuto: statusAuto ?? this.statusAuto,
      statusMessage: statusMessage ?? this.statusMessage,
    );
  }

  Bpi get lastBpi {
    if (bpis.length > 1) {
      return bpis[bpis.length - 2];
    }
    return const Bpi();
  }

  double get getBalanceBTC {
    switch (type) {
      case CurrencyType.USD:
        return balance / bpis.last.bpi.usd.rateFloat;
      case CurrencyType.GBP:
        return balance / bpis.last.bpi.gbp.rateFloat;
      case CurrencyType.EUR:
        return balance / bpis.last.bpi.eur.rateFloat;
      default:
        return balance / bpis.last.bpi.usd.rateFloat;
    }
  }
}
