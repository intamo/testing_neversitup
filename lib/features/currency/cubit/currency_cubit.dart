import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'dart:async';
import 'package:testing_project/core/api/coin_desk/collections/get_current_price.dart';
import 'package:testing_project/core/enum/currency_type.dart';
import 'package:testing_project/core/enum/network_status.dart';
import 'package:testing_project/core/models/bpi/bpi.dart';

part 'currency_state.dart';

class CurrencyCubit extends Cubit<CurrencyState> {
  CurrencyCubit() : super(const CurrencyState.init());

  void onFetch() async {
    fetchBpi();

    /// Set auto fetch every 1 min
    Timer.periodic(const Duration(seconds: 60), (timer) => autoFetchBpi());
  }

  void autoFetchBpi() async {
    emit(state.copyWith(statusAuto: NetWorkStatus.loading, statusMessage: 'Loading'));
    try {
      final Bpi bpi = await getBpi();
      emit(state.copyWith(
        bpis: [...state.bpis, bpi],
        statusAuto: NetWorkStatus.success,
        statusMessage: 'Success',
      ));
    } catch (error) {
      emit(state.copyWith(
        statusAuto: NetWorkStatus.error,
        statusMessage: error.toString(),
      ));
    }
  }

  void fetchBpi() async {
    emit(state.copyWith(status: NetWorkStatus.loading, statusMessage: 'Loading'));
    try {
      final Bpi bpi = await getBpi();
      emit(state.copyWith(
        bpis: [...state.bpis, bpi],
        status: NetWorkStatus.success,
        statusMessage: 'Success',
      ));
    } catch (error) {
      emit(state.copyWith(
        status: NetWorkStatus.error,
        statusMessage: error.toString(),
      ));
    }
  }

  void onUpdateBalance(double balance) {
    emit(state.copyWith(balance: balance));
  }

  void onCurrencyTypeChange(CurrencyType type) {
    emit(state.copyWith(type: type));
  }
}
