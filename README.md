
# This is testing project for Neversitup

เป็นโปรเจคไว้สำหรับเทส Neversitup นะครับ :>


## Installation

ให้เปลี่ยนไฟล์ `.env.example` เป็น `.env` เฉยๆนะครับ
นอกนั้นก็ลงแบบปกติได้เลย


## About The Project

โครงสร้างหลักๆของโปรเจคนี้ใช้เป็น BLOC เป็นหลักนะครับ\
จะแบ่งออกเป็น 2 ส่วนใหญ่ๆ ได้แก่
- core
- feature

#### Core
ส่วนของ core จะเป็น Sharing component ประกอบไปด้วย
- api
- enum
- model
- BLOC
- extension
- widgets
- theme
- hive
- service
  อาจะมีมากกว่านี้ก็ได้ แล้วแต่เนื้องานอีกที

#### Feature
ส่วนของ feature จะเป็น Feature ภายใน application นะครับ ข้างจะประกอบไปด้วย
- cubit หรือ BLOC
- screen
- widgets
## About Me
สวัสดีครับ ผมชื่อเล่นว่า เป้ อายุ 25 ปี ครับ \
ปัจจุบันทำงานเป็น Mobile developer ใช้ Flutter เป็นหลักครับ


## Contact

Phone: +66 94-710-2772\
Email: pisit.intamo@gmail.com\
\
Have a nice day kub :>